### OmniBoard
Projeto final do curso de Programação do Instituto de Tecnologia ORT
Buscamos nesse projeto levar o quadro da sala de aula para a casa dos alunos de maneira ágil, prática e de baixo custo. 

O sistema elaborado necessita de um dispositivo android para tirar as fotos do quadro. As fotos são tiradas de maneira automatizada e periódica e ficam instantaneamente disponíveis numa rede social criada por nós, que foi moldada especificamente para o uso de professores e alunos. O projeto também abrange um filtro de duplicatas.