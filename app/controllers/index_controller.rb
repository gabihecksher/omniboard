class IndexController < ApplicationController
  def index
    if teacher_signed_in?
      redirect_to teachers_home_url
    end
    if student_signed_in?
      redirect_to student_home_url
    end
  end
end