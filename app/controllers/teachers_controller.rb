class TeachersController < ApplicationController
# before_action :set_teacher, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_teacher!, except: [:index]


  def home
    @teachers = Teacher.all
    @subjects = Subject.all
  end
  
  
  # GET /teachers
  # GET /teachers.json
  def index
    @teachers = Teacher.all
    @subjects = Subject.all
  end


  # GET /teachers/1
  # GET /teachers/1.json
  def show
    @subjects = Subject.all
  end

  # GET /teachers/new
  def new
    byebug
    @teacher = Teacher.new
    @subjects = Subject.all
  end

  # GET /teachers/1/edit
  def edit
    @subjects = Subject.all
  end
  
  def home
    @teacher = current_teacher
    @subjects = Subject.all
  end

  # POST /teachers
  # POST /teachers.json
  def create
    byebug
    @teacher = Teacher.new(teacher_params)

    respond_to do |format|
      if @teacher.save
        format.html { redirect_to @teacher, notice: 'Teacher was successfully created.' }
        format.json { render :show, status: :created, location: @teacher }
      else
        format.html { render :new }
        format.json { render json: @teacher.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /teachers/1
  # PATCH/PUT /teachers/1.json
  def update
    respond_to do |format|

      if @teacher.update(teacher_params)
        format.html { redirect_to @teacher, notice: 'Teacher was successfully updated.' }
        format.json { render :show, status: :ok, location: @teacher }
      else
        format.html { render :edit }
        format.json { render json: @teacher.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @teacher.destroy
    respond_to do |format|
      format.html { redirect_to teachers_url, notice: 'Teacher was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_teacher
      @teacher = Teacher.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def teacher_params
      byebug
      params.require(:teacher).permit(:name, :email, :avatar)
    end
    
end
