class Eventstarter
  @queue = :events

  def self.perform()
  	# MyClass.where("DAYOFWEEK(created_at) == 3")

	#events = Event.where("strftime('%Y-%m-%d %H:%M:%S', start) BETWEEN ? AND ?", Time.now, Time.now + 30.seconds)
	events = Event.where("start <= ? AND strftime('%w %H:%M:%S', start) BETWEEN ? AND ?", Time.now, (Time.now-5.seconds).strftime('%w %H:%M:%S'), (Time.now+30.seconds).strftime('%w %H:%M:%S'))
	events.each do |event|
		# system("./event_trigger teologia 10 2 192.168.1.9:8080 &") 
		# subject_id: 2, capture_interval: 20, duration: 200, start: "2015-09-04 19:38:00", repeat: nil, created_at: "2015-09-04 19:38:23", updated_at: "2015-09-04 19:38:23", day: 4, ip: "192.168.1.9:8080"
		subject_name = Subject.find(event.subject_id).name
		interval = event.capture_interval
		duration = event.duration
		ip = event.ip
		# puts "./event_trigger #{subject_name} #{duration} #{interval} #{ip}" 
		system("./event_trigger #{subject_name} #{duration} #{interval} #{ip} &")

	end
    
  end
end