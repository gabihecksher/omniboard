json.array!(@events) do |event|
  json.extract! event, :id, :subject_id, :capture_interval, :duration, :start, :day, :ip
  json.url event_url(event, format: :json)
end
