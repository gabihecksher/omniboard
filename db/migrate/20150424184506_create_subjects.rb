class CreateSubjects < ActiveRecord::Migration
  def change
    create_table :subjects do |t|
      t.string :name
      t.string :grade
      t.string :time
      t.string :weekday

      t.timestamps null: false
    end
  end
end
