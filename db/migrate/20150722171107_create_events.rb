class CreateEvents < ActiveRecord::Migration
  def change
    create_table :events do |t|
      t.integer :subject_id
      t.integer :capture_interval
      t.integer :duration
      t.datetime :start
      t.string :repeat, :limit => 1

      t.timestamps null: false
    end
  end
end
