import requests
# import shutil
import time
import os
import math

path = "images"

def define_intervalo_foto():
    while True:
        # Get user input
        num = raw_input('Intervalo Desejado entre fotos( em segundos ): ')

        # Try to convert it to a float
        try:
            num = float(num)
        except ValueError:
            print('Entre um numero, por favor.\n')
            continue
        return num
        break

def define_intervalo_aula():
    while True:
        # Get user input
        num = raw_input('Insira a duracao da aula ( em segundos ): ')

        # Try to convert it to a float
        try:
            num = float(num)
        except ValueError:
            print('Entre um numero, por favor.\n')
            continue
        return num
        break

duracao_total = define_intervalo_aula()
intervalo = int(define_intervalo_foto())
numero_de_fotos = int(round(duracao_total/intervalo))

contador = 0
while contador < numero_de_fotos:
    filename = str(time.time()) + '.jpg'
    fullpath = os.path.join(path, filename)


    r = requests.get('http://10.1.1.195:8080/photo.jpg', stream=True)
    if r.status_code == 200:
        with open(fullpath, 'wb') as f:
            for chunk in r.iter_content(1024):
                f.write(chunk)

    contador = contador + 1
    print(intervalo)
    time.sleep(intervalo)