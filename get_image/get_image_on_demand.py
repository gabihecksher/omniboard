__author__ = 'carolageiman'
import requests
import time
import os
from PIL import Image
import numpy
import sys


path = "images"

def compare(imgA, imgB):
    img1 = Image.open(imgA)
    img2 = Image.open(imgB)

    if img1.size != img2.size or img1.getbands() != img2.getbands():
    	return -1

    s = 0
    for band_index, band in enumerate(img1.getbands()):
    	m1 = numpy.fft.fft2(numpy.array([p[band_index] for p in img1.getdata()]).reshape(*img1.size))
    	m2 = numpy.fft.fft2(numpy.array([p[band_index] for p in img2.getdata()]).reshape(*img2.size))
    	s += numpy.sum(numpy.abs(m1-m2))

    return s


def define_intervalo_foto():
    while True:
        # Get user input
        num = raw_input('Intervalo Desejado entre fotos( em segundos ): ')

        # Try to convert it to a float
        try:
            num = float(num)
        except ValueError:
            print('Entre um numero, por favor.\n')
            continue
        return num
        break

def define_intervalo_aula():
    while True:
        # Get user input
        num = raw_input('Insira a duracao da aula ( em segundos ): ')

        # Try to convert it to a float
        try:
            num = float(num)
        except ValueError:
            print('Entre um numero, por favor.\n')
            continue
        return num
        break

def prompt_interativo():
    i = 0
    ip = raw_input('Digite o ip do seu dispositivo android, conectado na rede local')
    real_path = 'http://' + ip + '/photo.jpg'
    subdir = raw_input("Digite o nome do evento( o nome sera dado a uma pasta, na qual as fotos vao ser armazenadas")
    if not os.path.exists(subdir):
        os.makedirs(subdir)



    while True:
        #Get user input

        command = raw_input('Digite seu comando( digite h para ver todos os comandos:')
        if command == 'help':
            print("Use \n c -> capturar foto \n q -> sair do terminal interativo \n t -> ativar uma interface para o timer \n ip para ver o ip inserido \n k para eliminar fotos muito similares")
        if command == 'ip':
            print(ip)
        if command == 'k':
            j = i
            differentEnough = raw_input('Digite o valor de similaridade maximo para nao considerar como duplicatas')
            differentEnough = int(differentEnough)



            while (j > 1):
                if int(compare(str(subdir)+ '/' + str(j) + '.jpg',str(subdir) + '/' + str(j-1) + '.jpg' )) > differentEnough:
                    print("keeping")
                    j -= 1
                if int(compare(str(subdir)+ '/' + str(j) + '.jpg',str(subdir) + '/' + str(j-1) + '.jpg' )) == differentEnough:
                    print("keeping")
                    j -= 1
                if int(compare(str(subdir) + '/' + str(j) + '.jpg',str(subdir) + '/' + str(j-1) + '.jpg' )) < differentEnough:
                    print(int((compare(str(subdir) + '/' + str(j) + '.jpg',str(subdir) + '/' + str(j-1) + '.jpg'))),differentEnough)
                    print("removing")
                    os.remove(str(subdir) + '/' + str(j) + '.jpg')
                    j -= 1

        if command == 'p':
            print(i)

        if command == 'c':
            i = i + 1
            filename = str(i) + '.jpg'
            fullpath = os.path.join(subdir, filename)
            r = requests.get(real_path, stream=True)
            if r.status_code == 200:
                with open(fullpath, 'wb') as f:
                    for chunk in r.iter_content(1024):
                        f.write(chunk)
            print("foto capturada")
            continue

        if command == 'q':
            print("obrigado por utilizar o terminal interativo")
            break

        if command =='t':
            duracao_total = define_intervalo_aula()
            intervalo = int(define_intervalo_foto())
            numero_de_fotos = int(round(duracao_total/intervalo))

            contador = 0
            while contador < numero_de_fotos:
                i = i + 1
                filename = str(i) + '.jpg'
                fullpath = os.path.join(subdir, filename)


                r = requests.get(real_path, stream=True)
                if r.status_code == 200:
                    with open(fullpath, 'wb') as f:
                        for chunk in r.iter_content(1024):
                            f.write(chunk)

                contador = contador + 1
                print(intervalo)
                time.sleep(intervalo)



prompt_interativo()

