import glob
import itertools
import requests
import os
from PIL import Image
import numpy
import sys
from scipy.misc import imread
from scipy.linalg import norm
from scipy import sum 
from scipy import average


def compare(imgA, imgB):
    img1 = Image.open(imgA)
    img2 = Image.open(imgB)

    if img1.size != img2.size or img1.getbands() != img2.getbands():
    	return -1

    s = 0
    for band_index, band in enumerate(img1.getbands()):
    	m1 = numpy.fft.fft2(numpy.array([p[band_index] for p in img1.getdata()]).reshape(*img1.size))
    	m2 = numpy.fft.fft2(numpy.array([p[band_index] for p in img2.getdata()]).reshape(*img2.size))
    	s += numpy.sum(numpy.abs(m1-m2))

    return s

def compare_images(img1, img2):
    diff = img1 - img2  # elementwise for scipy arrays
    m_norm = sum(abs(diff))  # Manhattan norm
    return (m_norm)

def to_grayscale(arr):
    "If arr is a color image (3D array), convert it to grayscale (2D array)."
    if len(arr.shape) == 3:
        return average(arr, -1)  # average over the last axis (color channels)
    else:
        return arr

def killTooSimilar():
    #rootdir = '/Users/francisco/Desktop/resque-tryout/testederuby/public/'
    rootdir = os.environ["DB_PHOTO_PATH"]
    rootdir = rootdir + "/"
    for subdir, dirs, files in os.walk(rootdir):
        for file1, file2 in itertools.combinations(files,2):
            if file1.endswith(".jpg") and file2.endswith(".jpg"):
                if "avatars" not in subdir:
                    print os.path.join(subdir, file1), os.path.join(subdir, file2)
                    img1 = to_grayscale(imread(os.path.join(subdir, file1)).astype(float)) 
                    img2 = to_grayscale(imread(os.path.join(subdir, file2)).astype(float))
                    n_m = compare_images(img1, img2)
                    if n_m > 462531.757345:
                    	print("keeping a1")
                    if n_m == 462531.757345:
                    	print("keeping a2")
                    if n_m < 462531.757345:
                    	print n_m, 462531.757345
                    	print("removee")
                    	os.remove(os.path.join(subdir, file1))

killTooSimilar()