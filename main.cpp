#include <string>
#include <vector>
#include <iostream>
#include <curl/curl.h>
#include <fstream>
#include <sys/stat.h>
#include <stdlib.h>
#include <unistd.h>
#include <cstdio>
#include <ctime>

using namespace std;


inline bool exists_test1 (const std::string& name)
{
    if (FILE *file = fopen(name.c_str(), "r")) {
        fclose(file);
        return true;
    } else {
        return false;
    }
}

int startEvent(string subject, int durationInSeconds, int intervalInSeconds, string ip)
{
    
    int iterator = 0;
    int counter = durationInSeconds/intervalInSeconds;
    char* pPath;
    pPath = getenv ("DB_PHOTO_PATH");
    if (pPath == NULL)
        printf ("Seu arquivo .bashsrc precisa ser configurado: %s",pPath);

    string dirCheck = pPath;
    dirCheck += "/";
    dirCheck += subject;
    if( exists_test1(dirCheck) == false)
    {
        string newDir;
        newDir += "public/";
        newDir += subject;
        mkdir(newDir.c_str(),0777);
    }
    while( iterator < counter)
    {
        CURL *image;
        CURLcode imgresult;
        FILE *fp = NULL;
        image = curl_easy_init();
        if( image ){
            
            // Open file
            std::time_t rawtime;
            std::tm* timeinfo;
            char buffer [160];
            
            std::time(&rawtime);
            timeinfo = std::localtime(&rawtime);
            
            std::strftime(buffer,160,"%Y-%m-%d-%H-%M-%S",timeinfo);
            std::puts(buffer);
            
            //string s = to_string(iterator);
            string name = subject +'/'+ buffer + ".jpg";
            //string relative =
            ofstream file;
            cout << name << endl;
            file.open(name);
            file.close();
            string real_ip = ip;
            string real_url = "http://";
            real_url += real_ip;
            real_url += "/photo.jpg";
            iterator++;
            string path = pPath;
            path += "/";
            path += name;
            cout << path << endl;
            
            // .substr(0, 10)path.substr(0, 10).c_str()
            fp = fopen( path.c_str(), "wb");
            //cout << fp << endl;
            //cout << path;
            if( fp == NULL ) cout << "Falha ao abrir a pasta";
            
            cout << "Capturando de " << real_url << endl;
            curl_easy_setopt(image, CURLOPT_URL,real_url.c_str());
            curl_easy_setopt(image, CURLOPT_WRITEFUNCTION, NULL);
            curl_easy_setopt(image, CURLOPT_WRITEDATA, fp);
            
            
            // Grab image
            imgresult = curl_easy_perform(image);
            cout << iterator << endl;
            if( imgresult ){
                cout << "Falha ao capturar a imagem!\n";
            }
            sleep(intervalInSeconds);
        }
        // Clean up the resources
        curl_easy_cleanup(image);
        // Close the file
        fclose(fp);
    }
    return 0;
    
}
int main(int argc, char *argv[])
{
    int duration, interval;
    string subject;
    string ip;
    if( argc < 5 )
    {
        printf("faltam argumentos abençoado, assim tu me quebra");
        return -1;
    }
    subject = argv[1];
    duration = atoi(argv[2]);
    interval = atoi(argv[3]);
    ip = argv[4];
    
    
    printf("%d",startEvent(subject, duration, interval, ip));
    
    //printf("%d",startEvent("famb", 10, 2, "10.1.1.181:8080"));
    //mkdir("famb",0777);
    
    return 0;
}

